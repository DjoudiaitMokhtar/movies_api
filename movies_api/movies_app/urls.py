from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from movies_app import views


urlpatterns = [
    path('api/', views.Movie_list.as_view(), name='home'),
    path('api/detail/', views.Detail.as_view(), name='detail'),
    path('api/search/', views.Search.as_view(), name='search'),
    path('api/login/', views.Login.as_view(), name='login'),
    path('api/logout/', views.Logout.as_view(), name='logout'),
    path('api/register/', views.UserCreate.as_view(), name='account-create'),
]