import imdb
import json
from telnetlib import STATUS
from .serializers import MovieSerializers, UserSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response as DRF_response
from rest_framework.authtoken.models import Token
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password

from .models import  Movie


class Movie_list(APIView):
    """View to display the list of films in our catalog"""
    permission_classes = (IsAuthenticated,)            

    def get(self, request):
        movies = Movie.objects.all()
        serializer = MovieSerializers(movies, many=True)
        return Response(serializer.data, status=200)


class Search(APIView):
    """View to search for a new movie for our catalog via the imdb API"""
    permission_classes = (IsAuthenticated,)            
    def get(self, request):

        try:
            title_movie = request.query_params.get('title_movie')
            ia = imdb.IMDb()
            movie = ia.search_movie(title_movie)
            movies = []
            for i in range(len(movie)):
                data = {'id': movie[i].movieID,
                        'title': movie[i]['title'],
                        'poster': movie[i]['cover url']}
                movies.append(data)
        except KeyError:
            return Response(status=status.HTTP_404_NOT_FOUND)
    
        return Response(movies)
  

class Detail(APIView):
    """Class handles:

     - Displaying of movie details
     - Adding of movies for the movie catalog 
     - Deleting of movies for the movie catalog
    """
    permission_classes = (IsAuthenticated,)            
    def get(self, request):
        
        
        try:
            movie_id = request.query_params.get('id')
            ia = imdb.IMDb()
            movie = ia.get_movie(movie_id)
            title_movie = movie.get('title')
            poster_movie = movie.get('cover url')
           
            runtime_movie =[]
            for runtime in movie['runtime']:
                runtime_movie.append(runtime)
                
            synopsis_movie = []  
            for synopsis in movie['synopsis']:
                synopsis_movie.append(synopsis)
                
            movies = {'movie_id': movie_id,
                    'title': title_movie,
                    'poster': poster_movie,
                    'synopsis':synopsis_movie[0],
                    'runtime':runtime_movie[0]
            }
            
            directors = []
            for director in movie['directors']:
                directors.append({'movie_id': movie_id,
                                'director_name': director['name'],
                               })
          
            producers = []
            for producer in movie['producers']:
                producers.append({'movie_id': movie_id,
                                    'producer_name': producer['name'],
                                    })
          
            actors = []
            for actor in movie['actors']:
                actors.append({'movie_id': movie_id,
                                'actor_name': actor['name'],
                              })
                
        except KeyError:
            return Response(status=status.HTTP_404_NOT_FOUND)
        
       
        # Validate each item
        list_directors = []
        for director in directors:
            list_directors.append(director)
            
        list_producers = []
        for producer in producers:    
           list_producers.append(producer)
            
        
        list_actors = []   
        for actor in actors:
            list_actors.append(actor)

        return Response({'movie':movies,
                        'director':list_directors,
                        'producer':list_producers,
                        'actor':list_actors
                        },
                        status=200)
      
    def post(self, request):
        
        try:
            movie_id = request.query_params.get('id')
            ia = imdb.IMDb()
            movie = ia.get_movie(movie_id)
            title_movie = movie.get('title')
            poster_movie = movie.get('cover url')
           
            runtime_movie =[]
            for runtime in movie['runtime']:
                runtime_movie.append(runtime)
                
            synopsis_movie = []  
            for synopsis in movie['synopsis']:
                synopsis_movie.append(synopsis)
                
            movies = {'movie_id': movie_id,
                    'title': title_movie,
                    'poster': poster_movie,
            }
            
        except KeyError:
            return Response(status=status.HTTP_404_NOT_FOUND)
        try:
           
            serializer_movie = MovieSerializers(data=movies)
            serializer_movie.is_valid(raise_exception=True)
            serializer_movie.save()
            
            return Response({'movie':serializer_movie.data},
                                status=status.HTTP_201_CREATED)       
        
        except KeyError:
            return Response(status=status.HTTP_404_NOT_FOUND)
    
    def delete(self, request):
        
        try:
            movie_id = request.query_params.get('id')
            movie = Movie.objects.get(movie_id=movie_id)
        except Movie.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
       
        movie.delete()
        data = {'success': 'The movie id deleted'}
        return Response(data=data, status=status.HTTP_204_NO_CONTENT)

class UserCreate(APIView):
    """The class handles the registration of a user"""
    permission_classes = (AllowAny,)    
    def post(self, request, format='json'):
        print(request.data)
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.create(user=user)
                json = serializer.data
                json['token'] = token.key
                return Response(json, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Login(APIView):
    """Class handles user's login"""
    permission_classes = (AllowAny,)      
    def post(self, request, format=None):
        data = {}
        reqBody = json.loads(request.body)
        email_adress = reqBody['email']
       
        password = reqBody['password']
        try:
            Account = User.objects.get(email=email_adress)
        except KeyError:
            return Response(status = status.HTTP_400_BAD_REQUEST, data = "Incorrect Login credentials")


        token = Token.objects.get_or_create(user=Account)[0].key
        if not check_password(password, Account.password):
            return Response(status = status.HTTP_400_BAD_REQUEST, data = "Incorrect Password")

        if Account:
            if Account.is_active:
                print(request.user)
                login(request, Account)
                data["message"] = "user logged in"
                data["email_address"] = Account.email

                Res = {"data": data, "token": token}

                return Response(Res,status.HTTP_200_OK)

            else:
                return Response(status = status.HTTP_400_BAD_REQUEST, data = 'Account not active')

        else:
            return Response(status_code = status.HTTP_400_BAD_REQUEST, data = 'Account doesnt exist')

        
        
class Logout(APIView):
    """Class handles user's logout"""
    permission_classes = (IsAuthenticated,)      
    def get(self, request, format=None):
        # simply delete the token to force a login
        request.user.auth_token.delete()
        data = {'success': 'Successfully logged out'}
        return Response(data=data, status=status.HTTP_200_OK)
    
    
