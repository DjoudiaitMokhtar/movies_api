from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import RequestsClient, APIRequestFactory
from rest_framework.test import APITestCase, APIClient


def create_user(self):
        self.client = APIClient()
        self.user = User(email='test2@gmail.com',username='test2',password="123abcd")
        self.user.set_password("abc5678abc")
        self.user.save()
        self.token = Token.objects.create(user=self.user)
    

class TestDetailView(APITestCase):
    
    def test_request_client_post(self):
    
        create_user(self)
        response = self.client.get("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, 200)
        
    def test_request_client_get(self):
    
        create_user(self)
        response = self.client.post("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.post("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, 201)
        
    def test_request_client_delete(self):
    
        create_user(self)
        self.client.post("/api/detail/?id=4434004")
        response = self.client.delete("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.client.post("/api/detail/?id=4434004")
        response = self.client.delete("/api/detail/?id=4434004")
        self.assertEqual(response.status_code, 204)
